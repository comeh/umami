open Jest
open Expect
open ReactTestingLibrary

test("Component renders", () =>
  <div style={ReactDOMRe.Style.make(~color="rebeccapurple", ())}>
    <h1> {ReasonReact.string("Heading")} </h1>
  </div>
  |> render
  |> container
  |> expect
  |> toMatchSnapshot
)
